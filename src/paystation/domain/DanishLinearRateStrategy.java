package paystation.domain;

public class DanishLinearRateStrategy implements RateStrategy {

	@Override
	public int calculateTime( int amount ) {
	    return amount * 7;
	  }

}
