package paystation.domain;

public class DaneCoinValidator implements CoinValidator {

	@Override
	public boolean validator(int coinValue) {
		switch ( coinValue ) {
	    case 1: return true; 
	    case 2: return true; 
	    case 5:return true; 
	    case 10: return true; 
	    case 20:return true; 
	    default:
	    	return false;
	    }
	}

}
