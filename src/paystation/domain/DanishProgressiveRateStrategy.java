package paystation.domain;

public class DanishProgressiveRateStrategy implements RateStrategy {

	@Override
	public int calculateTime(int amount) {
		int time;
		if (amount > 20)  {
			amount = amount - 20;
			time = amount * 5 + 120;
			return time;
		}
		else {
			time= 6 * amount;
			return time;
		}
	}

}
