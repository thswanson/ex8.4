package paystation.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestDanishProgressiveRate {
		  RateStrategy rs;

		  @Before public void setUp() {
		    rs = new DanishProgressiveRateStrategy();
		  }
		  
		  /** Test a single hour parking */
		  @Test public void shouldGive60Min() {
		    // First hour: $1.5
			  //
		    assertEquals( 60 /*minutes*/, rs.calculateTime(10) ); 
		  }
		  @Test
		  public void shouldBe120Mins() {
			  assertEquals(120 , rs.calculateTime(20));
		  }
		  
		  @Test
		  public void shouldBe125Mins() {
			  assertEquals(125 , rs.calculateTime(21));
		  }
		  @Test
		  public void shouldBe130Mins() {
			  assertEquals(130 , rs.calculateTime(22));
		  }
		  

		  
}
