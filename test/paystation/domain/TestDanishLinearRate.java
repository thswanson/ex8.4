package paystation.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestDanishLinearRate {
	@Test public void shouldDisplay120MinFor300cent() {
	    RateStrategy rs = new DanishLinearRateStrategy();
	    assertEquals( 300 * 7 , rs.calculateTime(300) ); 
	  }
}
