package paystation.domain;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TestDanishValidator {
	
	DaneCoinValidator cv;
	PayStationImpl ps;
	@Before
	public void setup() {
		this.cv = new DaneCoinValidator();
	}
	
	@Test
	public void testValidCoinsFromAB() {
		assertTrue(cv.validator(1));
		assertTrue(cv.validator(2));
		assertTrue(cv.validator(5));
		assertTrue(cv.validator(10));
		assertTrue(cv.validator(20));
	}

}
